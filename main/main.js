///////////////////////
// Example how to write an exporter for a file format.
// This exporter is writing toolpathes and any additional path attributes
// to a CLI ascii file
///////////////////////

'use strict';

var MODEL = requireBuiltin('bsModel');
var HATCH = requireBuiltin('bsHatch');
var HATCH_INFO = requireBuiltin('bsHatchInfo');
var PARAM = requireBuiltin('bsParam');
var BUILDPARAM = requireBuiltin('bsBuildParam');
var POLY_IT = requireBuiltin('bsPolylineIterator');
var LOCALIZER = require('localization/localizer.js');
var CLI = require('main/export_cli.js');


const UNIQUE_FILTER_ID = 'CLI-ASCII-EX-{TODO: Put a fresh GUID here to avoid conflicts with other exporter scripts}';

/** 
* Specify general information
* @param  aboutInfo  bsAboutInfo
*/
exports.about = function(aboutInfo)
{
   aboutInfo.addCommentLine("CLI file export");
   aboutInfo.addCommentLine("Autodesk GmbH");
   aboutInfo.addCommentLine("Copyright 2018");
};

/**
* The buildstyle declares its parameters to the main application. 
* These parameters can be modified by the user within the specified range.
* Parameters are always declared like this:
* "group-id", "name", "display string", min, max, default
*
* @param  parameter   bsBuildParam
*/
exports.declareParameters = function(parameter)
{
  // declare hatch parameter groups (volume/overhang)
  // later on parameters are declared within these groups
  // Parameter groups are always declared like this:
  // "group-id", "display string"
  
  // CLI Parameter group:
  // All exporter parameter should be of type nGroupPlatform because
  // the export is always applied to a platform containing parts and not to individual parts
  parameter.declareParameterGroup(
    "cli_format", 
    LOCALIZER.GetMessage('cli_format'), 
    "", 
    BUILDPARAM.nGroupDefaultFlags | BUILDPARAM.nGroupPlatform
  );
  
  // Output precision: 0 to 16 decimal places
  parameter.declareParameterInt(
    "cli_format", "coord_precision", LOCALIZER.GetMessage('coord_precision'), 0, 16, 5);
  
};

/**
*  Declare machine configuration
*
* @param  machineConfig   bsMachineConfig
*/
exports.declareMachine = function(machineConfig)
{
  machineConfig.setBuildstyleName("CLI");
  
  // This project is purely a file exporter so we don't care 
  // about the selected machine, material and layer thickness
  machineConfig.setMachineName("Generic Open Machine");
  machineConfig.addMaterialName("Generic");
  machineConfig.addLayerThickness(100);
};

/**
*  Declare attributes to be stored within exposure vectors
*
* @param  buildAttrib   bsBuildAttribute
*/
exports.declareBuildAttributes = function(buildAttrib)
{
  // Just for testing purposes in ATU:
  buildAttrib.declareAttributeInt("int1");
  buildAttrib.declareAttributeReal("real1");
};

/**
* the buildstyle declares its export filters to the main application
* "filter-id", "display string", "file extension"
*
* @param  exportFilter  bsExportFilter
*/
exports.declareExportFilter = function(exportFilter)
{
  exportFilter.declareFilterEx({
    'sFilterId' : UNIQUE_FILTER_ID,
    'sFilterName' : LOCALIZER.GetMessage('extended_ascii_cli_file'),
    'sFilterExtension' : 'cli',
    'nVersionMajor' : CLI.version.major,
    'nVersionMinor' : CLI.version.minor,
    'isMultifile' : false
  });
};

/**
* Prepare a part for calculation. Checking configuration
* and adding properties to the part
* @param  model   bsModel
*/
exports.prepareModelExposure = function(model)
{

};

/**
* Calculate the exposure data / hatch vectors for one layer of a part
* @param  modelData    bsModelData
* @param  hatchResult  bsHatch
* @param  nLayerNr      int
*/
exports.makeExposureLayer = function(modelData, hatchResult, nLayerNr)
{  
  // creating toolpathes just for testing purposes in ATU:
  
  var island_it = modelData.getFirstIsland(nLayerNr);
  while(island_it.isValid())
  {
    // part or support island
    var is_part = MODEL.nSubtypePart == island_it.getModelSubtype();
    var is_support = MODEL.nSubtypeSupport == island_it.getModelSubtype();

    var island = island_it.getIsland().clone();
    var hatch_paths = new HATCH.bsHatch();
    
    if(is_part){
      island.hatch(hatch_paths, 0.3, 45, 0);
    }
    else{
      island.hatch(hatch_paths, 0.1, 0, 0);
    }
    
    hatch_paths.setAttributeReal("real1", 45.0);
    hatch_paths.setAttributeInt("int1", 7);
    hatchResult.moveDataFrom(hatch_paths);
  
    island_it.next();
  }
  
  // process all open polylines on the layer
  // open polylines are usually support/fixtures
  var polyline_it = modelData.getFirstLayerPolyline(
    nLayerNr, POLY_IT.nLayerOpenPolylines);

  while(polyline_it.isValid())
  {         
    var is_part = MODEL.nSubtypePart == polyline_it.getModelSubtype();
    var is_support = MODEL.nSubtypeSupport == polyline_it.getModelSubtype();
    
    var polyline_hatch_paths = new HATCH.bsHatch();
    
    polyline_it.polylineToHatch(polyline_hatch_paths);
    
    polyline_hatch_paths.setAttributeReal("real1", 75.0);
    hatch_paths.setAttributeInt("int1", 8);
    hatchResult.moveDataFrom(polyline_hatch_paths);
    
    polyline_it.next();
  }  
};

/**
* Export exposure data to file
* @param  exportFile     bsFile
* @param  sFilter        string
* @param  modelData      bsModelData
* @param  progress       bsProgress
*/
exports.exportToFile = function(
  exportFile, 
  sFilter, 
  modelData, 
  progress)
{  
  if(UNIQUE_FILTER_ID != sFilter){
    throw new Error("Unsupported export filter");
  }
  
  // make it an ascii file containing part and fixtures exposure data
  var cli_exporter = new CLI.cliExport(1.0, CLI.options.ascii | CLI.options.buildPart | CLI.options.support | CLI.options.exposure
  );
  
  // Enable toolpath attributes export
  // Note: By adding this we are breaking the official CLI standard  
  cli_exporter.mExportAllAttributes = true;
  
  // set output precision
  cli_exporter.mOutputPrecision = PARAM.getParamInt("cli_format", "coord_precision");
  
  // do the export
  cli_exporter.exportCli(exportFile, modelData, progress);  
};

/**
* Export exposure data to given directory
* @param  exportDir      bsDirectory
* @param  sFilter        string
* @param  modelData      bsModelData
* @param  progress       bsProgress
*/
exports.exportToDirectory = function(
  exportDir, 
  sFilter, 
  modelData, 
  progress)
{
  throw new Error("Cannot export to a directory. A file path is required");
};

/**
* Define arbitrary additional properties.
* These properties can be read by the host program (e.g. ATU or Netfabb) directly.
*
* There are predefined properties located within adsk_main :
* lower_layers_dependence :
*   How many additional layers below influence the toolpath
*   result of a current layer. E.g. if overhang calculation is applied then
*   the toolpath of a current layer would depend on one or more layers below.
*   This information is important to ATU and Netfabb for creating
*   a correct toolpath preview for individual layers.
* upper_layers_dependence :
*   How many additional layers above influence the
*   toolpath result of a current layer.
* custom_thickness_allowed :
*   Custom layer thickness is supported.
*
* @param sMaterial    Material name string
* @param nThickness   Layer thickness
* @param properties   Property object
*/
exports.declareBuildstyleProperties = function(
  sMaterial,
  nThickness,
  properties)
{
  // Any layer thickness is allowed
  properties.adsk_main.custom_thickness_allowed = true;
};
